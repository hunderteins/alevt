static void
fmt_page(struct export *e, struct fmt_page *pg, struct vt_page *vtp)
{
    char buf[16];
    int x, y;
    u8 c, *p = vtp->data[0];
    pg->dbl = 0;
    sprintf(buf, "\2%3x.%02x\7", vtp->pgno, vtp->subno & 0xff);
    strncpy(p,buf,8);
    for (y = 0; y < H; y++)
    {
      /* two variables for each attribute 
       * new_attr  -- information got from the latest char
       * here_attr -- value of the attribute for this xy position
       * So, if the attribute char is "Set-At" 
       *        then  
       *            {
       *              here_attr=new_attr=attribute(latest_char);
       *              ... processing ... (use here_attr !!!)
       *              here_attr=new_attr; // <-- unneeded, 
       *                                  // but doesn't break the code
       *            }
       *        else   // Set-After 
       *            {
       *              new_attr=attribute(latest_char);
       *              ... processing ... (use here_attr !!!)
       *              here_attr=new_attr;
       *            }
       * Attributes persist until changed by another attribute
       *   or end of row
       */
      u8 held_mosaic_char=' ';
      u8 dbl=0; /* set to 1 for double height page 
		  * (is used at the end of row processing) */
      
      u8 new_gfx=0, here_gfx=0; // Start-of-row default condition
      u8 new_bg=0, here_bg=0;   // Start-of-row default condition
      u8 new_fg=7, here_fg=7;   // Start-of-row default condition
      u8 new_bln=0, here_bln=0;   // Start-of-row default condition
      u8 new_dbl=0, here_dbl=0;   // Start-of-row default condition
      u8 new_con=0, here_con=0;   // Start-of-row default condition
      u8 here_sep=0;  /*  Start-of-row default condition
			*  set/reset by 0x19,0x1a chars
			*  Both (0x19,0x1a) are Set-At
			*  so anyway (here_sep==new_sep) all the time
			*/
      u8 new_hld=0, here_hld=0;  
	
      //hold attribute:
      u8 held_sep=0;

      for (x = 0; x < W; ++x)
	{
	  pg->data[y][x].ch=' ';  // case 0x00 ... 0x1f
	  switch (c=*p++)
	    {
	    case 0x00 ... 0x07:      
	      //Alpha Colour Codes (foreground)
	      //Set-After
	      new_gfx=0;
	      new_fg=c;  // in this case (c == (c & 0x07))
	      new_con=0;
	      break;
	    case 0x08:
	      //Flash
	      //Set-After
	      new_bln=EA_BLINK;
	      break;
	    case 0x09:
	      //Steady
	      //Set-At
	      new_bln=here_bln=0;
	      break;
	    case 0x0a:
	      //End Box
	      //Set-After
	      //!!! not processed
	      break;
	    case 0x0b:
	      //Start Box  (used with End Box to mark visible 
	      //            part of page on transparent ones
	      //            eg. newsflash or subtitle )
	      //Set-After
	      //!!! not processed
	      break;
	    case 0x0c:
	      //Normal Size
	      //Set-At
	      new_dbl=here_dbl=0;
	      break;
	    case 0x0d:
	      //Double Height
	      //Set-After
	      dbl=1;
	      new_dbl=EA_DOUBLE;
	      break;
	    case 0x0e:
	      //Double Width (for Presentation Level 2.5 and 3.5)
	      //Set-After
	      //!!! not processed
	      break;
	    case 0x0f:
	      //Double Size (for Presentation Level 2.5 and 3.5)
	      //Set-After
	      //!!! not processed
	      break;
	    case 0x10 ... 0x17:
	      //Mosaic Colour Codes
	      //Set-After
	      new_gfx=EA_GRAPHIC;
	      new_fg= c & 0x07;
	      new_con=0;
	      break;
	    case 0x18:
	      //Conceal
	      //Set-At
	      here_con=new_con=EA_CONCEALED;
	      break;
	    case 0x19:
	      //Contiguous Mosaic Graphics
	      //Set-At
	      here_sep=0;
	      break;
	    case 0x1a:
	      //Separated Mosaic Graphics
	      //Set-At
	      here_sep=EA_SEPARATED;
	      break;
	    case 0x1b:
	      //ESC (or Switch) 
	      //Set-After
	      //!!! not processed
	      break;
	    case 0x1c:
	      //Black Background
	      //Set-At
	      here_bg=new_bg=0;
	      break;
	    case 0x1d:
	      //New Background
	      //Set-At
	      here_bg=new_bg=here_fg;
	      break;
	    case 0x1e:
	      //Hold Mosaics
	      //Set-At
	      here_hld=new_hld=1;
	      break;
	    case 0x1f:
	      //Release Mosaics
	      //Set-After
	      new_hld=0;
	      break;

	  // start of "... processing ..."
	      
	    default: //noncontrol characters 
	      pg->data[y][x].ch=c;
	      /* **************************** */
	      /* special treating of sep
	       * when in Hold-Mosaics mode 
	       * see the specs for details...
	       */
	      if (c & (1<<5))  //why "6" bit set is (1<<5) ????????
		{
		  held_mosaic_char=c;
		  held_sep=here_sep;
		}
	      if (here_hld) //when in "hold" set 'here_sep' here:
		pg->data[y][x].attr|=here_sep;
	      /* **************************** */
	    } //end of switch
	  
	  if (here_hld)  c=held_mosaic_char;
	  
	  if ((here_gfx) && ((c & 0xa0) == 0x20))
	    pg->data[y][x].ch= c + ((c & 0x40) ? 32 : -32);
	  
	  if ((here_con) && (not e->reveal))
	    pg->data[y][x].ch=' ';	  
	  
	  pg->data[y][x].fg=here_fg;
	  pg->data[y][x].bg=here_bg;
	  
	  if (here_hld) // special treating again...
	    pg->data[y][x].attr=(here_dbl|here_gfx|
				 here_bln|here_con);
	  else  // when not in "hold" set 'here_sep' here
	    pg->data[y][x].attr=(here_dbl|here_gfx|
				 here_bln|here_con|here_sep);
	  
	  // end of "... processing ..."
	  here_gfx=new_gfx;
	  here_fg=new_fg;
	  here_bg=new_bg;
	  here_bln=new_bln;
	  here_dbl=new_dbl;
	  here_con=new_con;
	  here_hld=new_hld;
	}
      if (dbl)
        {
	  pg->dbl |= 1 << y;
	  for (x = 0; x < W; ++x)
            {
	      if (~pg->data[y][x].attr & EA_DOUBLE)
		pg->data[y][x].attr |= EA_HDOUBLE;
	      pg->data[y+1][x] = pg->data[y][x];
	      pg->data[y+1][x].ch = ' ';
            }
	  y++;p+=40;
        }
    }
    
}
